+++
title = "{{ replace .Name `-` ` ` | title }}"
date = {{ .Date }}
lastmod = {{ .Date }}
summary = {{ .Summary }}
tags = []
categories = []
imgs = []
cover = ""  # image show on top
readingTime = false  # show reading time after article date
toc = false
comments = false
justify = false  # text-align: justify;
single = false  # display as a single page, hide navigation on bottom, like as about page.
license = "MIT"  # CC License
draft = true
+++
